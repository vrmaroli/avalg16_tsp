/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: vishnu
 *
 * Created on 17 October, 2016, 12:26 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <utility>
#include <cmath>
#include <climits>
#include <ctime>
#include <sys/time.h>

// #define TIME_LIMIT 1.95e-5
#define TIME_LIMIT 1.98 // in seconds, with some margin

using namespace std;

/*
 * 
 */

double get_wall_time() {
    struct timeval time;
    if (gettimeofday(&time,NULL)){
        //  Handle error
        return 0;
    }
    return (double)time.tv_sec + (double)time.tv_usec * .000001;
}
double get_cpu_time() {
    return (double)clock() / CLOCKS_PER_SEC;
}

int euclideanDistance(double x1, double y1, double x2, double y2) {
    return sqrt(pow(x1-x2, 2) + pow(y1-y2, 2));
}

int main(int argc, char** argv) {
    double cpu_now = get_cpu_time();
    double cpu_total_tic = cpu_now;
    double cpu_local_tic = cpu_now;
    double cpu_local_max = 0;
    int numberOfPoints;
    cin >> numberOfPoints;
    vector <pair<double, double> > points;
    int result[numberOfPoints];
    double x, y;
    
    for(int i = 0; i<numberOfPoints; i++) {
        cin >> x >> y;
        points.push_back(make_pair(x, y));
    }
    
    // Creating distance table
    int smallestX, smallestY;
    int tempDist = INT_MAX;
    int distance[numberOfPoints][numberOfPoints];
    for(int i = 0; i<numberOfPoints; i++) {
        for(int j = 0; j<numberOfPoints; j++) {
            if(i<j) {
                distance[i][j] = euclideanDistance(points[i].first, points[i].second, points[j].first, points[j].second);
                if(tempDist > distance[i][j]) {
                    tempDist = distance[i][j];
                    smallestX = i;
                    smallestY = j;
                }
            }
            else if(i==j) {
                distance[i][j] = 0;
            }
            else {
                distance[i][j] = distance[j][i];
            }
            // cerr << distance[i][j] << "\t";
        }
        // cerr << endl;
    }
    int tempScore = INT_MAX;
    for(int start = 0; start < numberOfPoints; start++) {
        vector <int> tempResult;
        bool chosen[numberOfPoints];
        for(int i = 0; i < numberOfPoints; i++) {
            chosen[i] = false;
        }
        int current = start, next;
        bool finished;
        while(true) {
            chosen[current] = true;
            tempResult.push_back(current);
            finished = true;
            for(int i = 0; i < numberOfPoints; i++) {
                if(!chosen[i]) {
                    finished = false;
                }
            }
            if(finished) {
                break;
            }
            tempDist = INT_MAX;
            for(int i = 0; i < numberOfPoints; i++) {
                if(not chosen[i] and tempDist > distance[current][i]) {
                    tempDist = distance[current][i];
                    next = i;
                }
            }
            current  = next;
        }
        int scoreResult = 0;
        for(int i = 0; i < tempResult.size()-1; i++) {
            scoreResult += distance[tempResult[i]][tempResult[i+1]];
        }
        scoreResult+=distance[*tempResult.rbegin()][*tempResult.begin()];
        if(scoreResult < tempScore) {
            tempScore = scoreResult;
            for(int i = 0; i < numberOfPoints; i++) {
                result[i] = tempResult[i];
            }
        }
        double cpu_now = get_cpu_time();
        double cpu_total_spent = cpu_now - cpu_total_tic;
        double cpu_local_spent = cpu_now - cpu_local_tic;
        if (cpu_local_spent > cpu_local_max){
          cpu_local_max = cpu_local_spent;
        }
        cpu_local_tic = cpu_now;

        if ((cpu_total_spent + cpu_local_max) > TIME_LIMIT){
          break;
        }
    }
    //cerr << "scoreResult: " << scoreResult << endl;
    for(int i = 0; i < numberOfPoints; i++) {
        cout << result[i] << endl;
    }
    return 0;
}
