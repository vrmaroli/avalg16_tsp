/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   2opt.cpp
 * Author: vishnu
 *
 * Created on 25 October, 2016, 12:04 AM
 */

#include <cstdlib>
#include <algorithm>
#include <iostream>
#include <vector>
#include <utility>
#include <cmath>
#include <climits>
#include <ctime>
#include <sys/time.h>

// #define TIME_LIMIT 1.95e-5
#define TIME_LIMIT 1.99 // in seconds, with some margin

using namespace std;

/*
 * 
 */

double get_wall_time() {
    struct timeval time;
    if (gettimeofday(&time,NULL)){
        //  Handle error
        return 0;
    }
    return (double)time.tv_sec + (double)time.tv_usec * .000001;
}
double get_cpu_time() {
    return (double)clock() / CLOCKS_PER_SEC;
}

int euclideanDistance(double x1, double y1, double x2, double y2) {
    return sqrt(pow(x1-x2, 2) + pow(y1-y2, 2));
}

int main(int argc, char** argv) {
    srand(time(NULL));
    double cpu_now = get_cpu_time();
    double cpu_total_tic = cpu_now;
    double cpu_local_tic = cpu_now;
    double cpu_local_max = 0;
    
    // reading input
    int numberOfPoints;
    cin >> numberOfPoints;
    vector <pair<double, double> > points;
    double x, y;    // some temporary variables
    for(int i = 0; i<numberOfPoints; i++) {
        cin >> x >> y;
        points.push_back(make_pair(x, y));
    }

    // Creating distance table
    int distance[numberOfPoints][numberOfPoints];
    for(int i = 0; i<numberOfPoints; i++) {
        for(int j = 0; j<numberOfPoints; j++) {
            if(i<j) {
                distance[i][j] = euclideanDistance(points[i].first, points[i].second, points[j].first, points[j].second);
            }
            else if(i==j) {
                distance[i][j] = 0;
            }
            else {
                distance[i][j] = distance[j][i];
            }
        }
    }
    
    // random initial path result
    vector <int> result;
    int score = 0;
//    for(int i=0; i<numberOfPoints; i++) {
//        result.push_back(i);
//    }
//    random_shuffle(result.begin(), result.end());
    
    // nearest neighbor start
    bool chosen[numberOfPoints];
    for(int i = 0; i < numberOfPoints; i++) {
        chosen[i] = false;
    }
    int current = 0, next, tempDist;
    bool finished;
    while(true) {
        chosen[current] = true;
        result.push_back(current);
        finished = true;
        for(int i = 0; i < numberOfPoints; i++) {
            if(!chosen[i]) {
                finished = false;
            }
        }
        if(finished) {
            break;
        }
        tempDist = INT_MAX;
        for(int i = 0; i < numberOfPoints; i++) {
            if(not chosen[i] and tempDist > distance[current][i]) {
                tempDist = distance[current][i];
                next = i;
            }
        }
        current = next;
    }

    for(int i=0; i<numberOfPoints-1; i++) {
        score += distance[result[i]][result[i+1]];
    }
    score += distance[result[numberOfPoints-1]][result[0]];
    
    int randIndex1, randIndex2;
    int tempScore, tempSwap;
    int a, b, c, d;
    
    // generating random index list
    vector <int> randIndex;
    for(int i=0; i<numberOfPoints; i++) {
        randIndex.push_back(i);
    }
    random_shuffle(randIndex.begin(), randIndex.end());
    
    int randIndexItertor = 0;
    int offset = 3;
    while(true) {
        if(randIndexItertor >= numberOfPoints) {
            randIndexItertor = 0;
            offset++;
        }
        
        randIndex1 = randIndex[randIndexItertor];
        randIndex2 = randIndex1 + offset;
        randIndex2 %= numberOfPoints;
        
        a = result[randIndex1];
        b = result[(randIndex1+1)%numberOfPoints];
        c = result[(randIndex2-1<0)?(numberOfPoints-1):(randIndex2-1)];
        d = result[randIndex2];
        
        if(randIndex1 < randIndex2 and randIndex2 - randIndex1 < 3) { // fix boundary conditions
            // continue;
        }
        else if (randIndex1 > randIndex2 and numberOfPoints - randIndex1 + randIndex2 < 3) {
            // continue;
        }
        else if (randIndex1 == randIndex2) {
            // continue;
        }
        else {
            tempScore = score;
            tempScore -= (distance[a][b] + distance[c][d]);
            tempScore += (distance[a][c] + distance[b][d]);

            if(tempScore < score) {
                score = tempScore;
                if(randIndex1 < randIndex2) {
                    /*
                    for(int i = randIndex1+1, j = randIndex2-1; i<j; i++, j--) {
                        tempSwap = result[i];
                        result[i] = result[j];
                        result[j] = tempSwap;
                    }
                     */
                    reverse(result.begin() + randIndex1+1, result.begin() + randIndex2-1);
                }
                else {
                    int i = 1;
                    int len = numberOfPoints - randIndex1 + randIndex2;
                    while(true) {
                        // exit control statements
                        if(i > len/2) {
                            break;
                        }
                        
                        tempSwap = result[(randIndex1 + i)% numberOfPoints];
                        result[(randIndex1 + i)% numberOfPoints] = result[(randIndex1 + len-i)% numberOfPoints];
                        result[(randIndex1 + len-i)% numberOfPoints] = tempSwap;
                        
                        // update statements
                        i++;
                    }
                    
                }
            }
        }
        
        randIndexItertor++;
        
        // time limit based break out
        double cpu_now = get_cpu_time();
        double cpu_total_spent = cpu_now - cpu_total_tic;
        double cpu_local_spent = cpu_now - cpu_local_tic;
        if (cpu_local_spent > cpu_local_max){
          cpu_local_max = cpu_local_spent;
        }
        cpu_local_tic = cpu_now;
        if ((cpu_total_spent + cpu_local_max) > TIME_LIMIT) {
          break;
        }
    }
    //cerr << "scoreResult: " << scoreResult << endl;
    for(int i = 0; i < numberOfPoints; i++) {
        cout << result[i] << endl;
    }
    return 0;
}
